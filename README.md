#  Margaret Kibi (`kibigo!`)  #

##  About Me  ##

I’m interested in designing and building tools for creating, cataloguing, and distributing {D·I·Y ∣ zine ∣ folk ∣ &amp;·c} resources for the people.  In the past, I’ve worked in social media in the context of the [Mastodon‐compatible](https://joinmastodon.org) [ActivityPub Fediverse](https://activitypub.rocks), and a lot of my personal projects involve X·M·L or R·D·F.

A more detailed personal profile can be found at [`https://go.KIBI.family/About/#me`](https://go.KIBI.family/About/#me), which is my preferred I·R·I for use with Linked Data.  Much of my older work is on GitHub under the user [`marrus-sh`](https://github.com/marrus-sh).

##  Crib Sheet  ##

 +  **Referring to me :—**

    +  I sign my code `kibigo!`; feel free to treat this as a [“nick”](http://xmlns.com/foaf/spec/#term_nick) or casual style.  I will not be offended if you refer to me as such.

    +  My personal name (<i>Margaret</i>) can be abbreviated <i>Margô</i> or <i>Gô</i>.  Many of my friends refer to me just as <i>Kibi</i>, and in formal situations (e·g among strangers), this is my preference.  My preferred title is <i>Lady</i> (e·g <i>Lady Margaret Kibi</i>), but if you live in a context where that implies certain rank or privilege, <i>Ms</i> works just as well.

    +  I am a woman and you should refer to me in a way that your culture deems acceptable for referring to women (regarding pronouns, etcetera).  I do not take offence at being referred to in nongendered terms, provided consistent application of a rule (e·g regardless of {cis‐ ∕ trans‐}gender status).

 +  **Contact :—**

    +  My Twitter home is [@1024xGO](https://twitter.com/1024xGO) (opinions are my own); I do check it, even if I don’t often post.
    
    +  Please only use email for {professional ∣ work‐related} business.

___

*With love,*\
<img alt="KIBIGO" height="32" src="https://go.KIBI.family/Images/Logotype/image.svg"/>
